CREATE TABLE `tasks`
(
    `taskId`       INT AUTO_INCREMENT PRIMARY KEY,
    `title`        VARCHAR(100) NOT NULL,
    `description`  VARCHAR(255) NOT NULL,
    `dueDate`      DATETIME     NOT NULL,
    `dateCreated`  DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `createdBy`    INT  DEFAULT NULL,
    `dateModified` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `modifiedBy`   INT  DEFAULT NULL,
    `active`       INT NOT NULL DEFAULT '1'
);

insert into tasks values ('3', 'Implement JWT Validation', 'This is to include the authentication service', '2024-05-16 23:00:00', '2024-03-19 18:23:55', NULL, '2024-03-19 18:26:00', NULL, '1');
insert into tasks values ('4', 'Implement JWT Validations', 'This is to include the authentication service', '2024-05-16 23:00:00', '2024-03-19 18:23:55', NULL, '2024-03-19 18:26:00', NULL, '1');
insert into tasks values ('5', 'Implement JWT Validations', 'This is to include the authentication service', '2024-05-16 23:00:00', '2024-03-19 18:23:55', NULL, '2024-03-19 18:26:00', NULL, '1');
insert into tasks values ('6', 'Implement JWT Validations', 'This is to include the authentication service', '2024-05-16 23:00:00', '2024-03-19 18:23:55', NULL, '2024-03-19 18:26:00', NULL, '1');
insert into tasks values ('7', 'This is a water test', 'This is to include the authentication service', '2024-05-16 23:00:00', '2024-03-19 18:23:55', NULL, '2024-03-19 18:26:00', NULL, '1');
insert into tasks values ('8', 'water tasks', 'This is to include the authentication service', '2024-05-16 23:00:00', '2024-03-19 18:23:55', NULL, '2024-03-19 18:26:00', NULL, '1');