package io.ipsl.taskservices.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.ipsl.taskservices.configs.ApplicationProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.SecurityWebFiltersOrder;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.authentication.AuthenticationWebFilter;
import org.springframework.security.web.server.context.NoOpServerSecurityContextRepository;
import reactor.core.publisher.Mono;

@Configuration
@RequiredArgsConstructor
@EnableWebFluxSecurity
public class ReactiveSecurityConfiguration {
    private final ObjectMapper objectMapper;
    private final ApplicationProperties applicationProperties;


    @Bean
    SecurityWebFilterChain springSecurityWebFilterChain(ServerHttpSecurity http) {

        return http
                .httpBasic(ServerHttpSecurity.HttpBasicSpec::disable)
                .headers(ServerHttpSecurity.HeaderSpec::disable)
                .formLogin(ServerHttpSecurity.FormLoginSpec::disable)
                .csrf(ServerHttpSecurity.CsrfSpec::disable)
                .logout(ServerHttpSecurity.LogoutSpec::disable)
                .securityContextRepository(NoOpServerSecurityContextRepository.getInstance())
                .exceptionHandling(exceptionHandlingSpec -> {
                    exceptionHandlingSpec.accessDeniedHandler((exchange, denied) -> Mono.error(denied));
                    exceptionHandlingSpec.authenticationEntryPoint(new InvalidAuthorizationReactiveHandler(objectMapper));
                })
                .addFilterAt(preJWTAuthenticationFilter(), SecurityWebFiltersOrder.AUTHENTICATION)
                .authorizeExchange(authorizeExchangeSpec -> {
                    authorizeExchangeSpec
                            .pathMatchers("v1/tasks/**","/swagger-ui/**", "/v3/api-docs/**", "/swagger-ui.html").permitAll()
                            .anyExchange()
                            .authenticated();
                })
                .build();
    }


    public AuthenticationWebFilter preJWTAuthenticationFilter() {
        final AuthenticationWebFilter authenticationWebFilter = new
                AuthenticationWebFilter(new JWTAuthenticationManager(applicationProperties, objectMapper));
        authenticationWebFilter.setServerAuthenticationConverter(new JWTAuthenticationFilter());
        return authenticationWebFilter;
    }

}
