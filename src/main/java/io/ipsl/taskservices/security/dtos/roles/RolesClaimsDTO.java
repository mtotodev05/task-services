package io.ipsl.taskservices.security.dtos.roles;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class RolesClaimsDTO implements Serializable {
    private List<GroupDTO> groups;

    public String toString() {
        return new Gson().toJson(this);
    }
}
