package io.ipsl.taskservices.services;

import io.ipsl.taskservices.constants.Constants;
import io.ipsl.taskservices.dtos.requests.CreateTaskDTO;
import io.ipsl.taskservices.dtos.response.ResponseArray;
import io.ipsl.taskservices.entities.Tasks;
import io.ipsl.taskservices.exceptions.TasksNotFoundException;
import io.ipsl.taskservices.repository.TaskRepository;
import io.ipsl.taskservices.repository.specs.TaskSpecification;
import io.ipsl.taskservices.responsemanager.ResponseManager;
import io.ipsl.taskservices.utils.Utilities;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Service
@RequiredArgsConstructor
@Slf4j
public class TaskServiceImpl implements TaskService {
    private final Utilities utilities;
    private final TaskRepository taskRepository;
    private final ResponseManager responseManager;

    @Override
    public Mono<ResponseEntity<ResponseArray>> onCreateTask(CreateTaskDTO createTaskDTO) {
        log.info("We are about to create a task with the details {}", createTaskDTO);
        // Let's build the entity object.
        var task = Tasks.builder()
                .title(createTaskDTO.getTitle())
                .description(createTaskDTO.getDescription())
                .dueDate(this.utilities.convertDateTime(createTaskDTO.getDueDate())).build();

        // Lets create the task async.
        return Mono.fromCallable(() -> this.taskRepository.save(task))
                .subscribeOn(Schedulers.boundedElastic())
                .flatMap(tasks -> this.responseManager
                        .handleResponse(Constants.CREATE.toString(), tasks));
    }

    @Override
    public Mono<ResponseEntity<ResponseArray>> onFetchTaskById(int taskId) {
        return Mono.fromCallable(() -> this.taskRepository.findById(taskId))
                .subscribeOn(Schedulers.boundedElastic())
                .flatMap(tasks -> {
                    if (tasks.isEmpty()) {
                        return Mono.error(new
                                TasksNotFoundException("We could not find any tasks with the provided id"));
                    }
                    return this.responseManager.handleResponse(Constants.READ.toString(), tasks.get());
                });
    }

    @Override
    public Mono<ResponseEntity<ResponseArray>> onDeleteTaskById(int taskId) {
        return Mono.fromCallable(() -> this.taskRepository.findById(taskId))
                .subscribeOn(Schedulers.boundedElastic())
                .flatMap(tasks -> tasks.map(value -> Mono.fromCallable(() -> {
                                    this.taskRepository.delete(value);
                                    return true;
                                }).subscribeOn(Schedulers.boundedElastic())
                                .then(this.responseManager.handleResponse(Constants.DELETE.toString(), null)))
                        .orElseGet(() -> Mono.error(new
                                TasksNotFoundException("We could not find any tasks with the provided id"))));
    }

    @Override
    public Mono<ResponseEntity<ResponseArray>> onUpdateTask(int taskId, CreateTaskDTO createTaskDTO) {
        return Mono.fromCallable(() -> this.taskRepository.findById(taskId))
                .subscribeOn(Schedulers.boundedElastic())
                .flatMap(tasks -> tasks.map(value -> Mono.fromCallable(() -> {
                                    var task = tasks.get();
                                    task.setDescription(createTaskDTO.getDescription());
                                    task.setTitle(createTaskDTO.getTitle());
                                    task.setDueDate(this.utilities
                                            .convertDateTime(createTaskDTO.getDueDate()));
                                    return this.taskRepository.save(task);
                                }).subscribeOn(Schedulers.boundedElastic())
                                .then(this.responseManager.handleResponse(Constants.UPDATE.toString(), null)))
                        .orElseGet(() -> Mono.error(new
                                TasksNotFoundException("We could not find any tasks with the provided id"))));
    }

    @Override
    public Mono<ResponseEntity<ResponseArray>> onFetchAllTasks(int page, int size,
                                                               String sortBy, String sortDirection) {

        Pageable pageable;
        if (sortBy != null && !sortBy.isEmpty()) {
            var sortDirect = Sort.DEFAULT_DIRECTION;
            if(sortDirection != null && !sortDirection.isEmpty()){
                sortDirect = Sort.Direction.fromString(sortDirection);
            }
            pageable = PageRequest.of(page, size, Sort.by(sortDirect,sortBy));
        } else {
            pageable = PageRequest.of(page, size);
        }
        return Mono.fromCallable(() -> this.taskRepository.findAll(pageable).getContent())
                .subscribeOn(Schedulers.boundedElastic())
                .flatMap(tasks -> this.responseManager.handleResponses(Constants.READALL.toString(),tasks));
    }

    @Override
    public Mono<ResponseEntity<ResponseArray>> onSearchTasks(String title, String description,
                                                             String dueDate, int page,
                                                             int size, String sortBy, String sortDirection) {
        Pageable pageable;
        if (sortBy != null && !sortBy.isEmpty()) {
            var sortDirect = Sort.DEFAULT_DIRECTION;
            if(sortDirection != null && !sortDirection.isEmpty()){
                sortDirect = Sort.Direction.fromString(sortDirection);
            }
            pageable = PageRequest.of(page, size, Sort.by(sortDirect,sortBy));
        } else {
            pageable = PageRequest.of(page, size);
        }

        return Mono.fromCallable(() -> {
            // Lets build the specification.
            final Specification<Tasks> tasksSpecification = Specification.where(TaskSpecification.title(title))
                    .and(TaskSpecification.description(description))
                    .and(TaskSpecification.dueDate(dueDate));
            return this.taskRepository.findAll(tasksSpecification,pageable).getContent();
        }).subscribeOn(Schedulers.boundedElastic()).flatMap(tasks -> this.responseManager.handleResponses(Constants.READALL.toString(),tasks));
    }
}
