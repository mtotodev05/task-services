package io.ipsl.taskservices.security.dtos.roles;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class GroupPermissionDTO {
    private Long permissionId;
    private String permissionScope;
    private String permissionDescription;
    private Long dateCreated;
}
