package io.ipsl.taskservices;


import io.ipsl.taskservices.dtos.requests.CreateTaskDTO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

@AutoConfigureWebTestClient(timeout = "360000")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TaskControllerTests {

    @Autowired
    private WebTestClient webClient;

    public static final String INVALID_JWT = "JhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTM1OTAyMzAsInN1YiI6ImpvaG4tZG9lLWFkbWluQGxvZ2ljZWEuY29tIiwidXNlcklkIjoxLCJlbWFpbCI6ImpvaG4tZG9lLWFkbWluQGxvZ2ljZWEuY29tIiwiZ3JvdXBzIjpbeyJ1c2VyR3JvdXBJZCI6MSwiZ3JvdXBzIjp7Imdyb3VwSWQiOjEsImdyb3VwTmFtZSI6IkFkbWluIEdyb3VwIiwiZ3JvdXBEZXNjcmlwdGlvbiI6IkFkbWluIEdyb3VwIENhcGFiaWxpdGllcyIsImRhdGVDcmVhdGVkIjoxNjkzMjUxNDc3MDAwLCJwZXJtaXNzaW9ucyI6W3sicGVybWlzc2lvbklkIjo0LCJwZXJtaXNzaW9uU2NvcGUiOiJBRE1JTiIsInBlcm1pc3Npb25EZXNjcmlwdGlvbiI6IkFkbWluIGNhcGFiaWxpdGllcyIsImRhdGVDcmVhdGVkIjoxNjkzNDIxMTEzMDAwfV19LCJkYXRlQ3JlYXRlZCI6MTY5MzI1MTg2MTAwMH1dLCJleHAiOjMxNjkzNTkwMjMwfQ.LQOAQ83CrKqbQFyL4CUmvlAQ6CelGmMfkVtjBFGJnFk";
    public static final String VALID_JWT = "eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTA4NjczMTMsInN1YiI6ImJyaWFuLmthbWF1Z0BnbWFpbC5jb20iLCJ1c2VySWQiOjMsImVtYWlsIjoiYnJpYW4ua2FtYXVnQGdtYWlsLmNvbSIsImdyb3VwcyI6W3sidXNlckdyb3VwSWQiOjMsImdyb3VwcyI6eyJncm91cElkIjoxLCJncm91cE5hbWUiOiJCYXNpYyBVc2VycyIsImdyb3VwRGVzY3JpcHRpb24iOiJBIEJhc2ljIHVzZXIgY2FuIGFkZCwgdXBkYXRlLCBmZXRjaCB0YXNrcyIsImRhdGVDcmVhdGVkIjoxNzEwNzY5ODg3MDAwLCJwZXJtaXNzaW9ucyI6W3sicGVybWlzc2lvbklkIjo2LCJwZXJtaXNzaW9uU2NvcGUiOiJDUkVBVEUiLCJwZXJtaXNzaW9uRGVzY3JpcHRpb24iOiJDcmVhdGUgUGVybWlzc2lvbiIsImRhdGVDcmVhdGVkIjoxNzEwNzcwMTQ3MDAwfSx7InBlcm1pc3Npb25JZCI6NywicGVybWlzc2lvblNjb3BlIjoiUkVBRCIsInBlcm1pc3Npb25EZXNjcmlwdGlvbiI6IlJlYWQgUGVybWlzc2lvbiIsImRhdGVDcmVhdGVkIjoxNzEwNzcwMTQ3MDAwfSx7InBlcm1pc3Npb25JZCI6OSwicGVybWlzc2lvblNjb3BlIjoiVVBEQVRFIiwicGVybWlzc2lvbkRlc2NyaXB0aW9uIjoiVVBEQVRFIFBFUk1JU1NJT04iLCJkYXRlQ3JlYXRlZCI6MTcxMDc3MDE0NzAwMH1dfSwiZGF0ZUNyZWF0ZWQiOjE3MTA3NzA0MzQwMDB9XSwiZXhwIjozMTcxMDg2NzMxM30.K_0W0jd3g_hGmv5GQorxrar_sL9ArwoWws98cUN087E";

    private static final String TASKS = "/tasks";


    @Test
    @DisplayName("Test that user cannot access the tasks if they are not authenticated")
    public void testThatUserCannotAccessTasksIfNotAuthenticated() {
        this.webClient.get()
                .uri(TASKS + "/1")
                .exchange()
                .expectStatus().isForbidden();
    }

    @Test
    @DisplayName("Test that user cannot access tasks given invalid jwt")
    public void testThatUserCannotAccessTasksGivenInvalidJwt() {
        this.webClient.get()
                .uri(TASKS + "/1")
                .header("Authorization", "Bearer " + INVALID_JWT)
                .exchange()
                .expectStatus().isForbidden()
                .expectBody()
        ;
    }

    @Test
    @DisplayName("Test that given valid jwt, user can fetch tasks.")
    public void testThatGivenValidJWTTokenUserCanFetchTasks() {
        this.webClient.get()
                .uri(TASKS + "/4")
                .header("Authorization", "Bearer " + VALID_JWT)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.status").isEqualTo("200")
                .jsonPath("$.data.taskId").isEqualTo("4")
                .jsonPath("$.data.title").isEqualTo("Implement JWT Validations");
    }
    // Lets test that we return no record found for missing tasks.
    @Test
    @DisplayName("Test that we return no record found if no tasks are found.")
    public void testThatWeReturnNoRecordFoundIfNoTasksAreFound() {
        this.webClient.get()
                .uri(TASKS + "/84")
                .header("Authorization", "Bearer " + VALID_JWT)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.errorCode").isEqualTo("We could not find any tasks with the provided id");
    }

    // Lets the creation of tasks.
    @Test
    @DisplayName("Test that validation fails if due date is not provided in the correct format")
    public void testThatValidationFailsIfDueDateIsNotProvidedCorrectly(){
        var createTask = CreateTaskDTO.builder()
                .title("This is a test task")
                .description("This is a valid description")
                .dueDate("a test date").build();

        this.webClient.post()
                .uri(TASKS)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(createTask))
                .header("Authorization", "Bearer " + VALID_JWT)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody()
                .jsonPath("$.errorCode").isEqualTo("Bad Request");
    }


    @Test
    @DisplayName("Test that validation fails if due date is not provided in the correct format")
    public void testThatATaskIsCreatedSuccessfully(){
        var createTask = CreateTaskDTO.builder()
                .title("This is a test task")
                .description("This is a valid description")
                .dueDate("12th May 2024 12:00pm").build();

        this.webClient.post()
                .uri(TASKS)
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(createTask))
                .header("Authorization", "Bearer " + VALID_JWT)
                .exchange()
                .expectStatus().isCreated()
                .expectBody()
                .jsonPath("$.status").isEqualTo("200")
                .jsonPath("$.data.title").isEqualTo("This is a test task");
    }

    // Lets test delete.
    @Test
    @DisplayName("Test that a task is deleted successfully")
    public void testThatATaskIsDeletedSuccessfully(){
        // Perform the deletion.
        this.webClient.delete()
                .uri(TASKS + "/5")
                .header("Authorization", "Bearer " + VALID_JWT)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.status").isEqualTo("200");

        // Confirm that the task was deleted successfully.
        this.webClient.get()
                .uri(TASKS + "/5")
                .header("Authorization", "Bearer " + VALID_JWT)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.errorCode").isEqualTo("We could not find any tasks with the provided id");
    }

    // Lets test update.
    @Test
    @DisplayName("Test that update of an existing task")
    public void testThatATaskIsUpdatedSuccessfully(){
        var createTask = CreateTaskDTO.builder()
                .title("This is a valid task title")
                .description("This is a valid description")
                .dueDate("12th Apr 2024 12:00pm").build();

        this.webClient.put()
                .uri(TASKS +"/3")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(createTask))
                .header("Authorization", "Bearer " + VALID_JWT)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.status").isEqualTo("200");

        this.webClient.get()
                .uri(TASKS + "/3")
                .header("Authorization", "Bearer " + VALID_JWT)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.status").isEqualTo("200")
                .jsonPath("$.data.title").isEqualTo("This is a valid task title");
    }

    // Lets test the fetching of data.
    @Test
    @DisplayName("Test that the user is able to fetch a list of tasks")
    public void testThatAUserCanFetchAListOfTasks() {
        this.webClient.get()
                .uri(TASKS + "?page=0&size=1&sortBy=dueDate&sortDirection=desc")
                .header("Authorization", "Bearer " + VALID_JWT)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.status").isEqualTo("200")
                .jsonPath("$.data.length()").isEqualTo("1");
    }


    // Lets implement search
    @Test
    @DisplayName("Test that the user is able to search  a list of tasks")
    public void testThatAUserCanSearchAListOfTasks() {
        this.webClient.get()
                .uri(TASKS + "/search?title=water&page=0&size=10&sortBy=dueDate&sortDirection=desc")
                .header("Authorization", "Bearer " + VALID_JWT)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.status").isEqualTo("200")
                .jsonPath("$.data.length()").isEqualTo("2");
    }
}
