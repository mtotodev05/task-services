package io.ipsl.taskservices.exceptions;

public class MissingJWTException extends Exception{
    public MissingJWTException(String exception){
        super(exception);
    }
}
