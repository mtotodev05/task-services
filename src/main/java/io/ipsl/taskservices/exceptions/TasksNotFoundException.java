package io.ipsl.taskservices.exceptions;

public class TasksNotFoundException extends Exception {
    public TasksNotFoundException(String message) {
        super(message);
    }
}
