package io.ipsl.taskservices.responsemanager;

import io.ipsl.taskservices.configs.ApplicationProperties;
import io.ipsl.taskservices.constants.Constants;
import io.ipsl.taskservices.dtos.response.ResponseArray;
import io.ipsl.taskservices.entities.Tasks;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class ResponseManager {

    private final ApplicationProperties applicationProperties;


    public Mono<ResponseEntity<ResponseArray>> handleResponse(String status,
                                                              Tasks data) {
        if (status.equalsIgnoreCase(Constants.CREATE.toString())) {
            var responseArray = ResponseArray.builder()
                    .status(String.valueOf(this.applicationProperties.getSuccessStatusCode()))
                    .description(this.applicationProperties.getCreateTaskSuccess())
                    .data(data)
                    .build();
            return Mono.just(ResponseEntity.created(URI.create("/tasks"
                    + data.getTaskId())).body(responseArray));
        }
        if (status.equalsIgnoreCase(Constants.READ.toString()) ||
                status.equalsIgnoreCase(Constants.UPDATE.toString())) {
            var responseArray = ResponseArray.builder()
                    .status(String.valueOf(this.applicationProperties.getSuccessStatusCode()))
                    .description(this.applicationProperties.getCreateTaskSuccess())
                    .data(data)
                    .build();
            return Mono.just(ResponseEntity.ok(responseArray));
        }
        if (status.equalsIgnoreCase(Constants.DELETE.toString())) {
            var responseArray = ResponseArray.builder()
                    .status(String.valueOf(this.applicationProperties.getSuccessStatusCode()))
                    .description(this.applicationProperties.getDeleteTaskSuccess())
                    .data(null)
                    .build();
            return Mono.just(ResponseEntity.ok(responseArray));
        }

        return null;
    }

    public Mono<ResponseEntity<ResponseArray>> handleResponses(String status,
                                                               List<Tasks> data) {
        if (status.equalsIgnoreCase(Constants.READALL.toString())) {
            var responseArray = ResponseArray.builder()
                    .status(String.valueOf(this.applicationProperties.getSuccessStatusCode()))
                    .description(this.applicationProperties.getCreateTaskSuccess())
                    .data(data)
                    .build();
            return Mono.just(ResponseEntity.ok(responseArray));
        }
        return null;
    }
}
