package io.ipsl.taskservices.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Component
@Slf4j
public class Utilities {
    public Date convertDateTime(String date) {
        try {
            date = date.replaceAll("(?<=\\d)(st|nd|rd|th)", ""); // Remove ordinal suffixes

            // Here we check if the due date follows our format.
            SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM yyyy h:mma");

            Date inputDate = dateFormat.parse(date);
            log.debug("Input date validated {}", inputDate);
            return inputDate;
        } catch (Exception exception) {
            log.error("An Error ocurred while attempting to parse the date");
            return null;
        }
    }
}
