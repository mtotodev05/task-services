package io.ipsl.taskservices.configs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties("io.ipsl.taskservices")
public class ApplicationProperties {
    private String successStatusCode;
    private String createTaskSuccess;
    private String deleteTaskSuccess;
    private String jwtSharedSecret;
}
