package io.ipsl.taskservices.repository.specs;

import io.ipsl.taskservices.entities.Tasks;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;

public class TaskSpecification {

    public static Specification<Tasks> dueDate(final String dueDate) {
        return (root, query, criteriaBuilder) -> {
            if (dueDate == null || dueDate.isEmpty()) {
                return criteriaBuilder.and();
            }
            final Expression<String> dueDateExpression = criteriaBuilder.lower(root.get("dueDate"));
            Predicate predicate = criteriaBuilder
                    .like(dueDateExpression,criteriaBuilder.lower(criteriaBuilder.literal(dueDate + "%")));
            query.distinct(true);
            return predicate;
        };
    }

    public static Specification<Tasks> title(final String taskTitle) {
        return (root, query, criteriaBuilder) -> {
            if (taskTitle == null || taskTitle.isEmpty()) {
                return criteriaBuilder.and();
            }
            final Expression<String> titleExpression = criteriaBuilder.lower(root.get("title"));
            Predicate predicate = criteriaBuilder
                    .like(titleExpression,criteriaBuilder.lower(criteriaBuilder.literal("%"+ taskTitle + "%")));
            query.distinct(true);
            return predicate;
        };
    }

    public static Specification<Tasks> description(final String taskDescription) {
        return (root, query, criteriaBuilder) -> {
            if (taskDescription == null || taskDescription.isEmpty()) {
                return criteriaBuilder.and();
            }
            final Expression<String> taskDescriptionExpression = criteriaBuilder.lower(root.get("description"));
            Predicate predicate = criteriaBuilder
                    .like(taskDescriptionExpression,criteriaBuilder.lower(criteriaBuilder.literal("%"+ taskDescription + "%")));
            query.distinct(true);
            return predicate;
        };
    }


}
