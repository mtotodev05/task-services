package io.ipsl.taskservices.controller;


import io.ipsl.taskservices.dtos.requests.CreateTaskDTO;
import io.ipsl.taskservices.dtos.response.ResponseArray;
import io.ipsl.taskservices.services.TaskService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping("/tasks")
public class TaskController {
    private final TaskService taskService;

    @PostMapping
    @Operation(summary = "Create a task", description = "This endpoint will create a task")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Task Successfully Created."),
    })
    public Mono<ResponseEntity<ResponseArray>> createTask(@RequestBody @Valid CreateTaskDTO createTaskDTO) {
        return this.taskService.onCreateTask(createTaskDTO);
    }

    @GetMapping("/{taskId}")
    @Operation(summary = "Fetch A Task Using the task id", description = "This endpoint will fetch a task using the task id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Task Fetched Successfully"),
    })
    public Mono<ResponseEntity<ResponseArray>> fetchTasks(@PathVariable(name = "taskId") int taskId) {
        return this.taskService.onFetchTaskById(taskId);
    }


    @DeleteMapping("/{taskId}")
    @Operation(summary = "Delete A Task Using the task id", description = "This endpoint will delete a task using the task id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Task Deleted Successfully"),
    })
    public Mono<ResponseEntity<ResponseArray>> deleteTask(@PathVariable(name = "taskId") int taskId) {
        return this.taskService.onDeleteTaskById(taskId);
    }

    @PutMapping("/{taskId}")
    @Operation(summary = "Update A Task Using the task id", description = "This endpoint will update a task using the task id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Task Updated Successfully"),
    })
    public Mono<ResponseEntity<ResponseArray>> createTask(@PathVariable(name = "taskId") int taskId,
                                                          @RequestBody @Valid CreateTaskDTO createTaskDTO) {
        return this.taskService.onUpdateTask(taskId, createTaskDTO);
    }



    @GetMapping
    @Operation(summary = "Fetch All Tasks", description = "This endpoint will fetch all tasks, Take advantage of the sorting")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Tasks Fetched Successfully"),
    })
    public Mono<ResponseEntity<ResponseArray>> fetchAllTasks(@RequestParam(name = "page", defaultValue = "0") int page,
                                                             @RequestParam(name = "size", defaultValue = "10") int size,
                                                             @RequestParam(name = "sortBy", required = false) String sortBy,
                                                             @RequestParam(name = "sortDirection", required = false) String sortDirection) {

        return this.taskService.onFetchAllTasks(page, size, sortBy, sortDirection);
    }


    @Operation(summary = "Search A Task Using title, description or due date", description = "This endpoint will search a task using the title, description")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Tasks Fetched Successfully"),
    })
    @GetMapping("/search")
    public Mono<ResponseEntity<ResponseArray>> searchTasks(@RequestParam(name = "title", required = false) String title,
                                                           @RequestParam(name = "description", required = false) String description,
                                                           @RequestParam(name = "dueDate", required = false) String dueDate,
                                                           @RequestParam(name = "page", defaultValue = "0") int page,
                                                           @RequestParam(name = "size", defaultValue = "10") int size,
                                                           @RequestParam(name = "sortBy", required = false) String sortBy,
                                                           @RequestParam(name = "sortDirection", required = false) String sortDirection) {

        return this.taskService.onSearchTasks(title, description, dueDate, page, size, sortBy, sortDirection);
    }

}