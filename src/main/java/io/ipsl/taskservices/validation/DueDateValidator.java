package io.ipsl.taskservices.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Slf4j
public class DueDateValidator implements ConstraintValidator<ValidateDueDate, String> {
    private static final DateTimeFormatter FORMATTER
            = DateTimeFormatter.ofPattern("d MMM yyyy h:mma");

    @Override
    public boolean isValid(String date, ConstraintValidatorContext constraintValidatorContext) {
        if (date == null || date.isBlank()) {
            // Do not accept null date values.
            return false;
        }
        try {
            // Here we check if the due date follows our format.
            date = date.replaceAll("(?<=\\d)(st|nd|rd|th)", ""); // Remove ordinal suffixes
            SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM yyyy h:mma");

            Date inputDate = dateFormat.parse(date);
            log.debug("Input date validated {}", inputDate);
            return true;
        } catch (Exception exception) {
            log.error("Error Occurred while attempting to validate the date {}", exception.getMessage());
            return false;
        }
    }
}
