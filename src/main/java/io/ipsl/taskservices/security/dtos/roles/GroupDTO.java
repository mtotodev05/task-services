package io.ipsl.taskservices.security.dtos.roles;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class GroupDTO implements Serializable {
    private Long groupId;
    private String groupName;
    private String groupDescription;
    private Long dateCreated;
    private List<GroupPermissionDTO> permissions;


}
