package io.ipsl.taskservices.services;

import io.ipsl.taskservices.dtos.requests.CreateTaskDTO;
import io.ipsl.taskservices.dtos.response.ResponseArray;
import org.springframework.http.ResponseEntity;
import reactor.core.publisher.Mono;

public interface TaskService {
    Mono<ResponseEntity<ResponseArray>> onCreateTask(CreateTaskDTO createTaskDTO);

    Mono<ResponseEntity<ResponseArray>> onFetchTaskById(int taskId);

    Mono<ResponseEntity<ResponseArray>> onDeleteTaskById(int taskId);

    Mono<ResponseEntity<ResponseArray>> onUpdateTask(int taskId,CreateTaskDTO createTaskDTO);

    Mono<ResponseEntity<ResponseArray>> onFetchAllTasks(int page, int size, String sortBy, String sortDirection);

    Mono<ResponseEntity<ResponseArray>> onSearchTasks(String title, String description, String dueDate,
                                                      int page, int size, String sortBy,String sortDirection);


}
