package io.ipsl.taskservices.dtos.requests;

import io.ipsl.taskservices.validation.ValidateDueDate;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class CreateTaskDTO implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    @NotNull(message = "The task title is a required field")
    private String title;
    @NotNull(message = "The task description is a required")
    private String description;

    @ValidateDueDate
    @Schema(name = "dueDate", example = "11th May 2024 11:00pm")
    private String dueDate;


}
