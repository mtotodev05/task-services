package io.ipsl.taskservices.constants;

public enum Constants {
    CREATE,
    READ,
    UPDATE,
    DELETE,
    READALL
}
