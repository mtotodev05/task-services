package io.ipsl.taskservices.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DueDateValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateDueDate {
    String message() default """
            Invalid Due Date Provided. The date should be provided in the following format
             1. Format - 11th May 2024 11:00pm\s""";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
