package io.ipsl.taskservices.repository;

import io.ipsl.taskservices.entities.Tasks;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends JpaRepository<Tasks,Integer>, PagingAndSortingRepository<Tasks,Integer>,
        QueryByExampleExecutor<Tasks>, JpaSpecificationExecutor<Tasks> {
}
