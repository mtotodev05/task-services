#/bin/bash
## Our Base Image.
baseJDKImage=briankamaug/amazoncorretto:v1.0.0-9c38d21b-amd64-gl
applicationName=task-services
applicationVersion=1.0.0

## Lets get the docker username and password.
dockerUsername=briankamaug
##Push and pull token
dockerPassword=dckr_pat_Li9YtbkkbvtWojEZpTktcrhB8fE

## First lets build the docker image for task services.
echo "docker build --build-arg BASE_IMAGE="$baseJDKImage" --build-arg APPLICATION_NAME="$applicationName" -t  briankamaug/$applicationName:v1.0.0-erfb1389-amd64-gl ."
docker build --build-arg BASE_IMAGE="$baseJDKImage" --build-arg APPLICATION_NAME="$applicationName" -t  briankamaug/$applicationName:v1.0.0-erfb1389-amd64-gl .

## Docker login
docker login -u $dockerUsername -p $dockerPassword

## Lets push it to the registry.
docker push briankamaug/$applicationName:v1.0.0-erfb1389-amd64-gl

## Lets now deploy our services to minikube.
## Deploy to Minikube.
## First We Create A Namespace.
minikube kubectl create namespace staging

## Then we Deploy Mysql to the Staging Namespace.
## Create Secret to store mysql root password.
minikube kubectl -- --namespace  staging create -f charts/mysql-secret.yaml

## Create Mysql Deployment.
minikube kubectl --  --namespace  staging create -f charts/mysql-deployment.yaml

## Create Mysql Service.
minikube kubectl --  --namespace staging create -f charts/mysql-service.yaml

## Sleep for a few to make sure the pod is deployed
sleep 5m

## We need to also create the authentication service.
# This is the service responsible for generation of jwt tokens.

## Create authentication Deployment.
minikube kubectl --  --namespace  staging create -f charts/authentication-services-deployment.yaml

## Create authentication Service.
## minikube kubectl --  --namespace staging create -f charts/authentication-service.yaml

## Lets then create our task services.
minikube kubectl --  --namespace  staging create -f charts/tasks-services-deployment.yaml

## Sleep for 5m to wait for pod to start running.
sleep 5m

## WE need to finally expose the deployment externally
minikube kubectl --  --namespace  staging expose deployment authentication-services --type=LoadBalancer --port=80 --target-port=8080
minikube kubectl --  --namespace  staging expose deployment $applicationName --type=LoadBalancer --port=80 --target-port=8080
