CREATE TABLE IF NOT EXISTS `groups` (
  `groupId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupName` varchar(55) NOT NULL,
  `groupDescription` varchar(100) NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`groupId`),
  KEY `idx_search_by_group_name` (`groupName`)
);

INSERT INTO `groups` VALUES (1,'Basic Users','A Basic user can add, update, fetch tasks','2024-03-18 13:51:27','2024-03-18 13:56:57',1),(2,'Elevated Users','An elevated user can do what a basic user can do, plus deletion of tasks.','2024-03-18 13:56:57','2024-03-18 13:56:57',1);


CREATE TABLE IF NOT EXISTS `permissions` (
  `permissionId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupId` int(10) unsigned NOT NULL,
  `permissionScope` enum('CREATE','READ','UPDATE','DELETE') NOT NULL,
  `permissionDescription` varchar(45) NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`permissionId`),
  KEY `fk_group_id` (`groupId`),
  KEY `idx_unique_groupID_permission_scope` (`groupId`,`permissionScope`),
  CONSTRAINT `fk_permissions_groups` FOREIGN KEY (`groupId`) REFERENCES `groups` (`groupId`) ON UPDATE CASCADE
);

INSERT INTO `permissions` VALUES (6,1,'CREATE','Create Permission','2024-03-18 13:55:47','2024-03-18 13:55:47',1),(7,1,'READ','Read Permission','2024-03-18 13:55:47','2024-03-18 13:55:47',1),(9,1,'UPDATE','UPDATE PERMISSION','2024-03-18 13:55:47','2024-03-18 13:55:47',1),(10,2,'CREATE','Create Permission','2024-03-18 13:58:19','2024-03-18 13:58:19',1),(11,2,'READ','Read Permissiom','2024-03-18 13:58:19','2024-03-18 13:58:19',1),(12,2,'DELETE','Delete Permission','2024-03-18 13:58:19','2024-03-18 13:58:19',1),(13,2,'UPDATE','Update Permission','2024-03-18 13:58:19','2024-03-18 13:58:19',1);


CREATE TABLE IF NOT EXISTS `users` (
  `userId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`userId`),
  KEY `idx_search_by_username` (`username`)
);

INSERT INTO `users` VALUES (3,'brian.kamaug@gmail.com','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','2024-03-18 14:00:00','2024-03-18 14:00:00',1),(4,'brian.kamau@gmail.com','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','2024-03-18 14:00:00','2024-03-18 14:00:00',1);

CREATE TABLE IF NOT EXISTS `tasks` (
  `taskId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `dueDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdBy` int(10) unsigned DEFAULT NULL,
  `dateModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modifiedBy` int(10) unsigned DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`taskId`),
  UNIQUE KEY `taskId_UNIQUE` (`taskId`),
  KEY `idx_search_by_title` (`title`),
  KEY `idx_search_by_description` (`description`),
  KEY `idx_search_by_due_date` (`dueDate`),
  KEY `fk_created_by_must_be_a_user_idx` (`createdBy`),
  KEY `fk_modified_by_must_be_a_user_idx` (`modifiedBy`),
  CONSTRAINT `fk_created_by_must_be_a_user` FOREIGN KEY (`createdBy`) REFERENCES `users` (`userId`) ON UPDATE CASCADE,
  CONSTRAINT `fk_modified_by_must_be_a_user` FOREIGN KEY (`modifiedBy`) REFERENCES `users` (`userId`) ON UPDATE CASCADE
);


CREATE TABLE `userGroups` (
  `userGroupId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `groupId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`userGroupId`),
  UNIQUE KEY `unique_userid_group_id` (`userId`,`groupId`),
  UNIQUE KEY `userId_UNIQUE` (`userId`),
  KEY `fk_user_groups_groups_idx` (`groupId`),
  CONSTRAINT `fk_user_groups_groups` FOREIGN KEY (`groupId`) REFERENCES `groups` (`groupId`) ON UPDATE CASCADE,
  CONSTRAINT `fk_user_groups_users` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON UPDATE CASCADE
);

INSERT INTO `userGroups` VALUES (3,3,1,'2024-03-18 14:00:34','2024-03-18 14:00:34',1),(4,4,2,'2024-03-18 14:00:34','2024-03-18 14:00:34',1);

CREATE TABLE `userTasks` (
  `userTaskId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `taskID` int(10) unsigned NOT NULL,
  `dateAssigned` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `assignedBy` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateModified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`userTaskId`),
  UNIQUE KEY `userTaskId_UNIQUE` (`userTaskId`),
  UNIQUE KEY `unique_user_id_task_id` (`userId`,`taskID`),
  KEY `idx_search_by_userID` (`userId`),
  KEY `idx_search_by_task_id` (`taskID`),
  KEY `idx_search_by_assigned_by` (`dateAssigned`),
  KEY `fk_assigned_by_users_idx` (`assignedBy`),
  CONSTRAINT `fk_assigned_by_users` FOREIGN KEY (`assignedBy`) REFERENCES `users` (`userId`) ON UPDATE CASCADE,
  CONSTRAINT `fk_task_id_user_tasks` FOREIGN KEY (`taskID`) REFERENCES `tasks` (`taskId`) ON UPDATE CASCADE,
  CONSTRAINT `fk_user_id_users` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION
);